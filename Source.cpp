#include <iostream>
#include <string>


using namespace std;

string text; // string for my text
int choicenum; // int for my choices
int Ending = 0; // sets the number of endings

string GetTextFromUser(); // text function
int GetNumbersFromUser(); // numbers functions
bool suit = false; // sets a bool for if you've been over to the suit
bool suitOn = false; // sets a bool if you put on the suit
bool port = false; // sets a bool for if you've been to the port

void Intro();
void Choices();
void Panels();
void Suit();
void Port();
void Door();
void End();

void main()
{
	Intro();
	Choices();


	system("pause");
}

string GetTextFromUser()
{
	getline(cin, text);
	return text;
}

int GetNumbersFromUser()
{
	string num; 
	
	getline(cin, num); // gets the line that the user types
	
	choicenum = stoi(num); // changes it to a int
	
	return choicenum; // returns that int
}

//cout << "\n";

void Intro()
{
	cout << "***Welcome to Your Space Mission***\n\n";
	cout << "'Good Morning Solder' You hear as you wake up groggy from a hyper-sleep you took what feels like moments ago.\n";
	cout << "'This is Your On-Board hyper intelligent AI system, Martin, I am here to help you with your time here.'\n";
	cout << "His voice is slightly robotic, but more human than you think it should be.\n";
	system("pause");
	system("CLS");
	cout << "You sit up and look around, there isn't much but a few control panels, space suit, glass port, and the door.\n";
	cout << "All these things seem familiar to you but slightly fuzzy.\n";
	system("pause");
	system("CLS");
	cout << "'Something has awoken you from hyper-sleep. I will run diagnostics on you now,' Martin says. There is a slight whirring noise and a laser flashes out hitting you in the face.\n";
	cout << "This laser moves down your entire body then back up to your face with a nice satisfying *DING*.\n";
	cout << "'SCAN COMPLETE!' Martin rings out in a much harsher voice than before. 'You seem to be fine other than minor confusion.'\n";
	cout << "Do you remember who you are?\n";
	cout << "1 - Yes\n";
	cout << "2 - No\n";
	GetNumbersFromUser();
	system("CLS");
		if (choicenum == 1)
		{
			cout << "'Well that's good, I am very glad for that'\n";
			cout << "'Can you say your name for me?'\n";
			GetTextFromUser();
			cout << "'Good job remembering that'\n";
			system("pause");
		}
		if(choicenum == 2)
		{
			cout << "'Well that's not good at all.' He says in a more afraid tone\n";
			cout << "'There is a very rare chance that you could have permanent memory loss from hyper-sleeping machines.'\n";
			cout << "'That is very rare and you were tested so I don't understand how this could have happened.'\n";
			cout << "'Do you by chance remember your name?'\n";
			cout << "'If not at least try.\n";
			GetTextFromUser();
			system("pause");
		}
	system("CLS");
	cout << "'Well " << text << " what do remember? Is it anything, somethings, or just nothing?'\n";
	cout << "You look around trying to grasp for anything in your memory but you can't actually get anything to come back fully\n";
	cout << "You see the control panel, space suit, glass port, and the door.\n";
	system("pause");
	system("CLS");
}
void Choices()
{
	cout << "What would you like to go to?\n";
	cout << "1 - Control Panels\n";
	cout << "2 - Space Suit\n";
	cout << "3 - Port\n";
	cout << "4 - The Door\n";
	GetNumbersFromUser();
	system("CLS");
	if (choicenum == 1)
	{
		Panels();
	}
	if (choicenum == 2)
	{
		Suit();
	}
	if (choicenum == 3)
	{
		Port();
		port = true;
	}
	if (choicenum == 4)
	{
		Door();
	}
	
}
void Panels()
{
	cout << "You go over to the panels scanning your hands over them trying to get any remembrance of what is happening\n";
	cout << "'Are these sparking anything in your mind?' Martin says clearly concerned\n";
	cout << "You look where you hand is and see three different buttons. Red, Green, and Yellow.\n";
	cout << "What would you like to do?\n";
	cout << "1 - Press the Red Button\n";
	cout << "2 - Press the Green Button\n";
	cout << "3 - Press the Yellow Button\n";
	cout << "4 - Don't Press Any Button\n";
	GetNumbersFromUser();
	system("CLS");
		if (choicenum == 1)
		{
			
			cout << "*Click* you push the button down not remembering what it does.\n";
			cout << "The lights turn off...\n";
			cout << "You turn the lights back on and go about your day\n";
		}
		if (choicenum == 2)
		{
			if(suitOn == true)
			{
				cout << "*Click* you push the button down not remembering what it does.\n";
				cout << "You hear the door starting to open and everything in the shuttle starts moving and alarms start going off.\n";
				cout << "You start getting pulled towards the now open airlock.\n";
				cout << "Before you know it you are outside looking at the shuttle you were in.\n";
				cout << "You have your suit on though this luckily has the tether still attached to it\n";
				cout << "You start pulling yourself in and once you make it back you get to the panel and press the button again.\n";
				cout << "You then wonder who in there right mind made the airlock button green.\n";
			}
			else
			{
				Ending = 1;
				cout << "*Click* you push the button down not remembering what it does.\n";
				cout << "You hear the door starting to open and everything in the shuttle starts moving and alarms start going off.\n";
				cout << "You start getting pulled towards the now open airlock.\n";
				cout << "Before you know it you are outside looking at the shuttle you were in.\n";
				cout << "You can hear Martin yelling for you before you pass out and die...\n";
				system("pause");
				End();
			}
		}
		if (choicenum == 3)
		{
			if(suitOn == true && port == true)
			{
				Ending = 2;
				cout << "*Click* you push the button down not remembering what it does.\n";
				cout << "From what you can tell nothing happens\n";
				cout << "Then all of a sudden the room starts to shake and glitch and buffer it seems.\n";
				cout << "'WHY HAVE YOU DONE THIS' Martin screeches from the screen across the shuttle\n";
				cout << "The whole shuttle begins to deform and become a green screen room with cameras everywhere.\n";
				cout << "This whole ting has been a ruse. You've been tricked. Bamboozled. \n";
				cout << "Maybe your wife is really out there and looking for you.\n";
				cout << "You did it.\n";
				system("pause");
				End();
				
			}
			else
			{
				cout << "*Click* you push the button down not remembering what it does.\n";
				cout << "From what you can tell nothing happens\n";
				cout << "'Maybe that button does nothing.' Martin says sounding perplexed as well.\n";
				cout << "Hmm maybe something special has to be done to make this button do something, you think\n";
			}
		}
		if (choicenum == 4)
		{
			cout << "You decide pressing the buttons you remember nothing about is probably not the best plan.\n";
		}
	system("pause");
	system("CLS");
	Choices();

}
void Suit()
{
	if (suit == false)
	{
		cout << "You walk over to your space suit, you only know it is yours because it has your name on it.\n";
		cout << "It seems like it would fit.\n";
		cout << "Would you like to put it on?\n";
		cout << "1 - Yes\n";
		cout << "2 - No\n";
		GetNumbersFromUser();
		system("CLS");
		if (choicenum == 1)
		{
			suit = true;
			suitOn = true;
			cout << "You slide the suit on very easily because it was perfectly tailored to you.\n";
			cout << "It fits you snug and completely puts yo into your own bubble from the rest of the ship.\n";
		}
		if (choicenum == 2)
		{
			cout << "You leave the suit hanging there in case you need it.\n";
		}
	}
	else
	{
		cout << "You already have your suit on silly there's nothing else there.\n";
	}
	system("pause");
	system("CLS");
	Choices();
}
void Port()
{
	port = true;
	cout << "You walk up to the glass port. You look out into the endless amount of void in front of you and see te splendors of the universe through a small chunk of glass keeping you from death. \n";
	cout << "You wonder how long you've really been out here and how much longer you will be.\n";
	cout << "You rethink all of your life which you can't exactly remember very well. A wife, some kids maybe.\n";
	cout << "Martin interrupts this time of bliss with 'Are you ok?' And in honest you don't know. Your life as you know it is in this little hunk of metal in space.\n";
	cout << "You can't remember much but you know you had a life and you want it back.\n";
	cout << "You must figure out how to get back to it\n";
	cout << "You also hear a small click under you hand as you turn away but you think nothing of it.\n";
	system("pause");
	system("CLS");
	Choices();
}
void Door()
{
	cout << "Its the door keeping you inside the shuttle keeping you from a harsh death.\n";
	cout << "What more do you want.\n";
	cout << "OOH OOH OOH THERE'S A STICKER...\n";
	system("pause");
	system("CLS");
	Choices();
}
void End()
{
	system("CLS");
	cout << "You got ending "<< Ending <<" Thank you for playing\n";
	cout << "Play again for more endings.\n";
	cout << "Would you like to play again?\n";
	cout << "1 - Yes\n";
	cout << "2 - No\n";
	GetNumbersFromUser();
	system("CLS");
	if (choicenum == 1)
	{
		suit = false;
		suitOn = false;
		port = false;
		main();
	}
	if (choicenum == 2)
	{
		exit(0);
	}
}
